/**
 * Register Autocomplete functions with fields.
 * This is not strictly using jQuery, like the rest of the CMS.
 */
(function($) {
    $(function() {

        // Configuration returned by TMDB API.
        var config = {};

        // Mapping of Form fields to returned data.
        // This can be extended to support various API (using different maps).
        // If field names get added/renamed or deleted in the MovieInfo.getCMSFields method, please update this map as well.
        var tmbdDetailsMap = {
            // Format is "form element ID": "data key" or [wrapper name, data key]
            "Form_EditForm_Title": "title",
            "Form_EditForm_MovieTitle": "title",
            "Form_EditForm_MenuTitle": "title",
            "Form_EditForm_Adult": ["adult", Number],
            "Form_EditForm_Budget": "budget",
            "Form_EditForm_Genre": ["genres", arrayToNames],
            "Form_EditForm_ImdbID": "imdb_id",
            "Form_EditForm_Overview": "overview",
            "Form_EditForm_Popularity": "popularity",
            "Form_EditForm_PosterURL": ["poster_path", getPosterUrl],
            "Form_EditForm_ProdCompany": ["production_companies", arrayToNames],
            "Form_EditForm_ProdCountry": ["production_countries", arrayToNames],
            "Form_EditForm_Released": "release_date",
            "Form_EditForm_Revenue": "revenue",
            "Form_EditForm_Runtime": "runtime",
            "Form_EditForm_TagLine": "tagline",
            "Form_EditForm_TmbdID": "id"
        };

        // Credits request mapper.
        var tmbdCreditsMap = {
            "Form_EditForm_Cast": ["cast", arrayToNames],
            "Form_EditForm_Director": ["crew", function(data) {return namesByJob("Director", data);}],
            "Form_EditForm_Producer": ["crew", function(data) {return namesByJob("Producer", data);}],
            "Form_EditForm_Editor": ["crew", function(data) {return namesByJob("Editor", data);}]
        };

        // Update form with given data according provided map.
        updateForm = function(data, map) {
            var datakey = "";
            for (var elemid in map) {
                if (map.hasOwnProperty(elemid)) {
                    // Get data key.
                    if(typeof map[elemid] === 'string') {
                        datakey = map[elemid];
                    } else {
                        datakey = map[elemid][0];
                    }

                    // Check if data key exist.
                    if (typeof data[datakey] === "undefined") {
                        continue;
                    }

                    // Check if wrapper set.
                    if (typeof map[elemid][1] === "function") {
                        $('#' + elemid).val(map[elemid][1](data[datakey]));
                    } else {
                        $('#' + elemid).val(data[datakey]);
                    }
                }
            }
        };

        // Utility function to convert array of objects with name attribute to comma separated string or names.
        function arrayToNames(arr) {
            var result = '';
            if (typeof arr != 'undefined') {
                result = arr.map(function(el){
                    return el.name;
                }).join(', ');
            }
            return result;
        };

        // Make URL of poster from API response and current configuration.
        function getPosterUrl(poster_path) {
            var poster_url = '';
            if (poster_path != '') {
                var cimg = config.images;
                var imgsize = (cimg.poster_sizes.indexOf('w342') !== -1) ? 'w342'
                        : cimg.poster_sizes[cimg.poster_sizes.length - 1];
                poster_url = cimg.base_url + imgsize + poster_path;
            }

            // Add poster preview.
            var preview = $("#Form_EditForm_PosterImagePreview");
            if (poster_url != '') {
                if (preview.length == 0) {
                    preview = $('<img id="Form_EditForm_PosterImagePreview" src="' + poster_url + '" width="100" style="display:block">');
                    $("#Form_EditForm_MovieTitle").after(preview);
                } else {
                    preview.attr('src', poster_url);
                }
            } else {
                preview.remove();
            }

            return poster_url;
        };

        // Get names from array of objects by job.
        function namesByJob(job, arr) {
            var result = '';
            if (typeof arr != 'undefined') {
                result = arr
                .filter(function(el){
                    if (el.job == job) {
                        return true;
                    }
                    return false;
                })
                .map(function(el){
                    return el.name;
                }).join(', ');
            }
            return result;
        }

        // Get API configuration.
        getApiConfig = function() {
            var input = $('.field.autocomplete input.text[name^="SearchTerm"]');
            $.ajax({
                url: input.data('source') + 'configuration',
                dataType: "jsonp",
                data: {
                    api_key: input.data('apikey')
                },
                success: function(data) {
                    config = data;
                }
            });
        };

        // Load autocomplete functionality when field gets focused.
        $('.field.autocomplete input.text[name^="SearchTerm"]').live('focus', function() {

            var input = $(this);

            // Prevent this field from loading itself multiple times.
            if(input.data('loaded') == 'true') {
                return;
            }

            input.data('loaded', 'true');

            // Get configuration.
            getApiConfig();
            //
            // load autocomplete into this field
            input.autocomplete({
                source: function( request, response ) {
                    // Call the remote API, add a wildcard to the search term (without checking if there
                    // might be a wildcard already.
                    $.ajax({
                        url: input.data('source') + 'search/movie',
                        dataType: "jsonp",
                        data: {
                            api_key: input.data('apikey'),
                            query: request.term + "*",
                            search_type: "ngram"
                        },
                        success: function( data ) {
                            // delete the content of autocomplete-dropdown
                            if (data.error != undefined) {
                                response ([]);
                                return;
                            }
                            // update the data list, setting the value property which will
                            // be used to populate the labels into the drop down.
                            $.each( data.results, function(key, value) {
                                if (value.release_date) {
                                    var year_ind = value.release_date.indexOf("-");
                                    var release_year = (year_ind !== -1) ? value.release_date.substr(0, year_ind)
                                        : value.release_date;
                                    value.value = value.title + " (" + release_year + ")";
                                }
                            });

                            // Return the data we received plus the added titles 'movie title (year)'.
                            response(data.results);
                        }
                    });
                },

                minLength: input.attr('data-min-length'),

                select: function( event, ui ) {

                    if (ui.item.id == undefined) {
                        // Remove invalid value, as it didn't match anything.
                        input.val("");
                        input.data("autocomplete").term = "";
                        return false;
                    }

                    if(ui.item) {
                        input.parent().find('input[name="TmdbID"]').val(ui.item.id);

                        // Get General details.
                        $.ajax({
                            url: input.data('source') + 'movie/' + ui.item.id,
                            dataType: "jsonp",
                            data: {
                                api_key: input.data('apikey')
                            },
                            success: function(data) {
                                updateForm(data, tmbdDetailsMap);
                            }
                        });

                        // Get Credits.
                        $.ajax({
                            url: input.data('source') + 'movie/' + ui.item.id + '/credits',
                            dataType: "jsonp",
                            data: {
                                api_key: input.data('apikey')
                            },
                            success: function(data) {
                                updateForm(data, tmbdCreditsMap);
                            }
                        });
                        return true;
                    }

                    // Provided ui element not an item, reset the value in the textfield as
                    // the value is not from the tmbd list.
                    input.val("");
                    input.data("autocomplete").term = "";
                    return false;
                }
            });
        });
    });
})(jQuery);