<?php
/**
 * Copyright (C) 2015  Valerii Kuznetsov <solival.xp@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class MovieInfoPageTest extends SapphireTest {
    protected static $fixture_file = 'MovieInfoPage.yml';

    public function setUp() {
        parent::setUp();
        Config::nest();

        // Create a test folders for each of the fixture references
        $folderIDs = $this->allFixtureIDs('Folder');

        foreach($folderIDs as $folderID) {
            $folder = DataObject::get_by_id('Folder', $folderID);

            if(!file_exists(BASE_PATH."/$folder->Filename")) mkdir(BASE_PATH."/$folder->Filename");
        }
    }

    public function tearDown() {
        Config::unnest();
        parent::tearDown();

        $fileIDs = $this->allFixtureIDs('Image');
        foreach($fileIDs as $fileID) {
            $file = DataObject::get_by_id('Image', $fileID);
            if($file && file_exists(BASE_PATH."/$file->Filename")) unlink(BASE_PATH."/$file->Filename");
        }
    }

    public function testGetApiKey() {
        $defKey = '123';
        if (defined('TMBD_API')) {
            $defKey = TMBD_API;
        } else {
            define('TMBD_API', $defKey);
        }

        // Defined key.
        Config::inst()->update('MovieInfoPage', 'api_key', '');
        $this->assertEquals($defKey, MovieInfoPage::get_api_key());

        // Config key.
        Config::inst()->update('MovieInfoPage', 'api_key', 'teSt');
        $this->assertEquals('teSt', MovieInfoPage::get_api_key());
    }

    public function testGetFileByURL() {
        $class = new ReflectionClass('MovieInfoPage');
        $method = $class->getMethod('getFileByURL');
        $method->setAccessible(true);

        $page = new MovieInfoPage();

        // Check new file.
        $mocknew = $this->mockFile('test_image_new.png');
        $new = $method->invokeArgs($page, array('http://example.com/t/p/w342/test_image_new.png', 'Test title'));
        $this->assertGreaterThan(0, $new->ID);
        $this->assertEquals('Test title', $new->Title);
        $this->assertStringEndsWith('test_image_new.png', $new->Name);
        unlink($mocknew);

        // Check no title.
        $mocktitle = $this->mockFile('test_image_no_title.png');
        $notitle = $method->invokeArgs($page, array('http://example.com/t/p/w342/test_image_no_title.png'));
        $this->assertGreaterThan(0, $notitle->ID);
        $this->assertStringEndsWith('test_image_no_title.png', $notitle->Title);
        $this->assertStringEndsWith('test_image_no_title.png', $notitle->Name);
        unlink($mocktitle);

        // Check existing file.
        $image = $this->objFromFixture('Image', 'imageExist');
        $mockexist = $this->mockFile('test_image.png');
        $id = $image->write();
        $exist = $method->invokeArgs($page, array('http://example.com/t/p/w342/test_image.png'));
        $this->assertEquals($id, $exist->ID);
        unlink($mockexist);
    }

    /**
     * @expectedException UnexpectedValueException
     */
    public function testGetFileByURLException() {
        $class = new ReflectionClass('MovieInfoPage');
        $method = $class->getMethod('getFileByURL');
        $method->setAccessible(true);

        $page = new MovieInfoPage();
        $method->invokeArgs($page, array('http://example.com/?nopath=true'));
    }

    /**
     * Creates empty file to skip API request
     * @param string $name File name
     */
    protected function mockFile($fileName) {
        $page = new MovieInfoPage();
        $basePath = Director::baseFolder() . DIRECTORY_SEPARATOR;
        $folder = Folder::find_or_make($page->config()->media_upload_folder);
        $relativeFilePath = $folder->Filename . $fileName;
        $fullFilePath = $basePath . $relativeFilePath;
        touch($fullFilePath);
        return $fullFilePath;
    }
}