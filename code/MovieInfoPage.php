<?php
/**
 * Copyright (C) 2015  Valerii Kuznetsov <solival.xp@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class MovieInfoPage extends Page {

    private static $description = 'Search and display movie information.';

    /**
     * @config
     */
    private static $api_key;

    /**
     * @config
     */
    private static $media_upload_folder;

    /**
     * @var array
     *
     * The content fields for the MovieInfo matches the important fields of the OpenMovie Database API.
     * The API returns mainly character strings and as at this stage no further processing is known to the
     * data, we store the data as provided by the API as varchar.
     */
    private static $db = array(
        "MovieTitle"  => "Varchar(1024)",
        "Adult"       => "Int(1)",
        "Budget"      => "Currency(14)",
        "Genre"       => "Varchar(1024)",
        "ImdbID"      => "Varchar(128)",
        "Overview"    => "Text", // Consider HTMLText
        "Popularity"  => "Varchar(10)",
        "PosterURL"   => "Varchar(1024)",
        "ProdCompany" => "Varchar(1024)",
        "ProdCountry" => "Varchar(128)",
        "Released"    => "Date",
        "Revenue"     => "Currency(14)",
        "Runtime"     => "Int",
        "TagLine"     => "Varchar(1024)",
        "Cast"        => "Text",
        "Director"    => "Varchar(1024)",
        "Editor"      => "Varchar(1024)",
        "Producer"    => "Varchar(1024)",
        "TmbdID"      => "Varchar(10)", // TMBD internal id
    );
    private static $has_one = array(
        'PosterImage' => 'Image'
    );

    private static $defaults = array('Adult' => 0);

    /**
     * Updates the CMS Form in the backend, generating an edit form for the movie details.
     *
     * As the Movie-page will solely show the movie details and no further content, the HTML-Content
     * editfield has been removed.
     *
     * This editform uses a new MovieInfoAutoComplete formfield which calls the omdbAPI to retrieve datasets
     * from the web service. @See MovieInfoAutoComplete for more details.
     * @return FieldList
     */
    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Add Movie Fields to the Main tab in the edit form (before the content area)
        $fields->addFieldsToTab('Root.Main', array(
            HeaderField::create(_t('MovieInfoPage.MOVIES', 'Movies')),
            MovieInfoAutoComplete::create("SearchTerm", _t('MovieInfoPage.SEARCH', 'Search'), "", 128)
                ->setAttribute('data-apikey', self::get_api_key())
                ->setAttribute('placeholder', _t('MovieInfoPage.SEARCHPLACEHOLDER', 'Please enter a movie name to search for...'))
                ->setDescription(_t('MovieInfoPage.SEARCHDESC', 'After choosing a movie, the details will be used as a template to complete this form.')),

            HeaderField::create('MovieDetailsHeader', _t('MovieInfoPage.MOVIEDETAILS', 'Movie Details'), 3),
            TextField::create('MovieTitle', _t('MovieInfoPage.MOVIETITLE', 'Movie Title'), $this->Title),
            TextField::create('TagLine', _t('MovieInfoPage.TAGLINE', 'Tag line'), $this->TagLine),

            //UploadField::create('PosterImage', _t('MovieInfoPage.POSTERIMAGE', 'Poster Image'), $this->PosterImage),
            FieldGroup::create(
                array(
                    TextField::create('Runtime', _t('MovieInfoPage.RUNTIME', 'Runtime'), $this->Runtime),
                    TextField::create('Adult', _t('MovieInfoPage.ADULT', 'Adult'), $this->Adult)
                )
            )->setTitle(_t('MovieInfoPage.DETAILS', 'Details')),

            FieldGroup::create(
                array(
                    TextField::create('ProdCountry', _t('MovieInfoPage.COUNTRY', 'Country'), $this->ProdCountry),
                    TextField::create('ProdCompany', _t('MovieInfoPage.COMPANIES', 'Production Companies'), $this->ProdCompany),
                    TextField::create('Released', _t('MovieInfoPage.RELEASED', 'Released'), $this->Released),
                )
            )->setTitle(_t('MovieInfoPage.RELEASEINFO', 'Release Info')),

            FieldGroup::create(
                    array(
                        TextField::create('Budget', _t('MovieInfoPage.BUDGET', 'Budget'), $this->Budget),
                        TextField::create('Revenue', _t('MovieInfoPage.REVENUE', 'Revenue'), $this->Revenue),
                    )
            )->setTitle(_t('MovieInfoPage.FINANCIAL', 'Financial')),

            TextField::create('Genre', _t('MovieInfoPage.GENRE', 'Genre'), $this->Genre),
            TextareaField::create('Overview', _t('MovieInfoPage.OVERVIEW', 'Overview'), $this->Overview),

            HeaderField::create('PeopleInvolvedHeader', _t('MovieInfoPage.PEOPLEINVOLVED', 'People Involved'), 3),
            TextField::create('Director', _t('MovieInfoPage.DIRECTOR', 'Director'), $this->Director),
            TextField::create('Producer', _t('MovieInfoPage.PRODUCER', 'Producer'), $this->Producer),
            TextField::create('Editor', _t('MovieInfoPage.EDITOR', 'Editor'), $this->Editor),
            TextField::create('Cast', _t('MovieInfoPage.CAST', 'Cast'), $this->Cast),

            HeaderField::create('PopularityHeader', _t('MovieInfoPage.POPULARITY', 'Popularity'), 3),
            TextField::create('Popularity', _t('MovieInfoPage.POPULARITY', 'Popularity'), $this->Popularity),

            HiddenField::create('ImdbID', 'ImdbID', $this->ImdbID),
            HiddenField::create('TmbdID', 'TmbdID', $this->TmbdID),
            HiddenField::create('PosterURL', 'PosterURL', $this->PosterURL),
                ), 'Content'
        );

        // Remove the content area (HTMLEditField)
        $fields->removeByName('Content');

        return $fields;
    }

    /**
     * Overwrites default behaviour onBeforeWrite
     *
     * This method sets the page name based on the selected MovieTitle. If no movie title exists,
     * it will retain the existing page title (and navigation labels).
     * It clears the URL Segment variable as the SiteTree::onBeforeWrite will determine a new url-segment
     * based on the new page tile (which is the movie title).
     */
    protected function onBeforeWrite() {
        if ($this->MovieTitle) {
            $this->Title = $this->MovieTitle;
            $this->URLSegment = '';
        }

        if ($this->PosterURL) {
            $this->PosterImageID = $this->getFileByURL($this->PosterURL, $this->MovieTitle)->ID;
        }

        parent::onBeforeWrite();
    }

    /**
     * Return TMDB API key
     * @return string
     * @throws UnexpectedValueException
     */
    public static function get_api_key() {
        $api_key = self::config()->api_key;
        if (empty($api_key)) {
            if (!defined('TMBD_API')) {
                throw new UnexpectedValueException("TMDB key is not set");
            }
            $api_key = TMBD_API;
        }
        return $api_key;
    }

    /**
     * Make file entry from provided URL
     * @param string $url URL to get file from
     * @param string $title Optional image title
     * @return Image or null if no image/image not found
     * @throws UnexpectedValueException
     */
    private function getFileByURL($url, $title = '') {
        $fileName = basename(parse_url($url, PHP_URL_PATH));
        if (empty($fileName)) {
            // TMBD API must always provide filename in path.
            throw new UnexpectedValueException("Invalid URL to TMDB poster image");
        }
        $basePath = Director::baseFolder() . DIRECTORY_SEPARATOR;
        $folder = Folder::find_or_make($this->config()->media_upload_folder);
        $relativeFilePath = $folder->Filename . $fileName;
        $fullFilePath = $basePath . $relativeFilePath;

        // Check if this file is already registered.
        if($found = DataObject::get_one('Image', "\"ParentID\" = '{$folder->ID}' AND \"Name\" = '{$fileName}'")) {
            return $found;
        }

        // Check if this file is already uploaded.
        if (!file_exists($fullFilePath)) {
            // download the file
            $fp = fopen($fullFilePath, 'w');
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FILE, $fp);
            $data = curl_exec($ch);
            curl_close($ch);
            fclose($fp);
        }

        // Register.
        $file = new Image();
        $file->ParentID = $folder->ID;
        $file->OwnerID = (Member::currentUser()) ? Member::currentUser()->ID : 0;
        $file->setName($fileName);
        $file->setFilename($relativeFilePath);
        $file->Title = ($title) ? $title : $fileName;
        $file->write();

        return $file;
    }

}

/**
 * Class MovieInfo_Controller
 *
 * This controller has been created (with no further feature enhancements) to enable SS_Viewer to render
 * the MovieInfo template out of the module folder.
 */
class MovieInfoPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();
        Requirements::css("movieinfo/css/style.css");
    }

    /**
     * Get year of release
     * @return string
     */
    public function Year() {
        if ($this->Released) {
            return substr($this->Released, 0, strpos($this->Released, "-"));
        }
    }

}
