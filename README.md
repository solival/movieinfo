# MovieInfo Module

## Introduction

The MovieInfo module provides an Movie Info Page type which will show information about a movie using its title.

This module initially based on silverstripe/movieinfo module and has following differences:

 * TheMovieDB API is used instead of OpenMovieDB
 * Added Support of API keys
 * Added Poster Image scrapping
 * Added preview of poster image
 * Better abstraction of JS that easies adding new movie API
 * Translation support
 * Added Unit tests coverage

## Requirements

 * SilverStripe 3.1

This module tested on SilverStripe 3.1.13 with Simple template.

## Installation

### Option 1: Create a new project

Assuming you have composer installed, create a new project via composer create-project

    sudo composer create-project silverstripe/installer [silverstripe-project]

Follow the SilverStripe installation instructions. Then add the module to your SilverStripe project

    git clone https://bitbucket.org/solival/movieinfo.git movieinfo

Run a dev-build in the browser (or using sake via command line):

    http://mysite.com/dev/build?flush=all

The new page type is now available and ready for use.

### Option 2: Add repository into your project composer.json file

In case you already have a project, managed by composer, and you would like to add this module via composer, open your project composer.json file and add the following text segments to it...

#### Add the repository

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/solival/movieinfo.git"
        }
    ],

#### Require the module

    "require": {
        "php": ">=5.3.2",
        [...]
        "solival/movieinfo": "*"
    },

Then you change into your project folder and run a composer update to retrieve the repository.

    composer update

Finally run a dev-build and then the Movie Info will be available and ready for use.

## Instructions
Sign up and login into https://www.themoviedb.org/ web site. Request and receive your API key.
Add API key to movieinfo/_config/config.yml.

    [...]
    MovieInfoPage:
      media_upload_folder: 'movieinfo'
      api_key: 'YOUR-API-KEY-HERE'

Log into the CMS and create a new page of the page-type 'Movie Info'. After successfully creating the page type, you will find the CMS form with a number of new fields. The page itself does not hold any content anymore and shows only the movie information. For that reason the Content edit-field has been removed.

### Movie Info

The Movie Info stores the regular SiteTree information(title, navigation, and URL). The field Search title performs request to the TheMovieDb.org API.
When you select one of the results, all available movie information will be added to a page.

If you search and select another movie, all information will be updated for new movie.

### Searching for Movies

Select the Search field in the 'Movies' section of the form. A request will be send and the top several results will be returned and shown in a drop down. You need to enter at least three characters before the search-request will be send to the remote API.

The more characters of title entered, the more relevant movies are listed in select field.

### Saving Movie Page

When the user saves the page (i.e. Save or Publish) the title of the movie (if entered) will become the Page's title,
navigation label and the URL segment will be updated as well.

### Viewing the Movie Info page

Once the page has been published, everyone (based on the permission settings of the page) can view the content in a basic layout style.

## Developer Notes

If you would like to use the module but need to change the template, simply create a layout template in your themes/[name]/template/Layout folder and call it MovieInfoPage.ss. This template will them be taken to render the page.

## Potential Enhancements

* Load image gallery (from supported images).
* Get images URL server-side directly from API.
* Add UploadField for Poster Image to allow user edit/override it by their own image.
* View original images in popup.
* Choose language of data.
* Abstract search API so that other movie databases are supported.
* Add behat tests.

## Quality

* At the stage, only unit tests are added.
* Function or behaviour tests have not been created.
* Module has been mainly tested against Chrome and Firefox, IE10.

## Bugtracker

Bugs are tracked on bitbucket.org ([movieinfo issues](https://bitbucket.org/solival/movieinfo/issues))

## License

    Copyright (C) 2015  Valerii Kuznetsov <solival.xp@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.