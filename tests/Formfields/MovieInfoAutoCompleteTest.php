<?php
/**
 * Copyright (C) 2015  Valerii Kuznetsov <solival.xp@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class MovieInfoAutoCompleteTest extends SapphireTest {

    public function setUp() {
            parent::setUp();
            Config::nest();
    }

    public function tearDown() {
            Config::unnest();
            parent::tearDown();
    }

    public function testGetAttributes() {
        $autocomplete = new MovieInfoAutoComplete('MovieSearch');
        $attrs = $autocomplete->getAttributes();

        $this->assertEquals('off', $attrs['autocomplete']);
        $this->assertGreaterThanOrEqual(2, $attrs['data-min-length']);
        $this->assertNotEmpty($attrs['data-source']);
        $this->assertEquals($autocomplete->getBaseURL(), $attrs['data-source']);
        $this->assertTrue($attrs['data-require-selection']);
        $this->assertEquals('__autocomplete', substr($attrs['name'], -14));
    }

    public function testType() {
        $autocomplete = new MovieInfoAutoComplete('MovieSearch');
        $this->assertEquals('autocomplete text', $autocomplete->Type());
    }

    public function testField() {
        $autocomplete = new MovieInfoAutoComplete('MovieSearch');

        Requirements::clear();
        $autocomplete->Field();
        $backend = Requirements::backend();

        // Check that jquery ui and autocomplete js are required.
        $jsfiles = $backend->get_javascript();
        $this->assertContains(THIRDPARTY_DIR . '/jquery-ui/jquery-ui.js', $jsfiles);
        $this->assertContains(MOVIEINFO_DIR . '/javascript/MovieInfoAutoComplete.js', $jsfiles);
    }

     public function testGetBaseURL() {
        $autocomplete = new MovieInfoAutoComplete('MovieSearch');

        // Get default base URL.
        $this->assertEquals(MovieInfoAutoComplete::API_BASE_URL, $autocomplete->getBaseURL());

        // Change settings of base URL.
        Config::inst()->update('MovieInfoAutoComplete', 'base_url', 'http://example.com/');
        $this->assertEquals('http://example.com/', $autocomplete->getBaseURL());
    }
}